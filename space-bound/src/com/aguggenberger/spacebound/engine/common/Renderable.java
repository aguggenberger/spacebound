package com.aguggenberger.spacebound.engine.common;

public interface Renderable {
	public void render(float delta);
}
