package com.aguggenberger.spacebound.resources;

public class SpaceBoundAtlas {
	public static final String GAME_ATLAS = "atlas/images.atlas";
	public static final String SPACE_SHIP_REGION = "playerShip1_red";
	public static final String METEOR_BROWN_BIG1_REGION = "meteorBrown_big1";
	public static final String METEOR_BROWN_BIG2_REGION = "meteorBrown_big2";
	public static final String METEOR_BROWN_BIG3_REGION = "meteorBrown_big3";	
	public static final String METEOR_BROWN_BIG4_REGION = "meteorBrown_big4";
	public static final String METEOR_BROWN_MED1_REGION = "meteorBrown_med1";
	public static final String METEOR_BROWN_MED3_REGION = "meteorBrown_med3";
	public static final String METEOR_BROWN_SMALL1_REGION = "meteorBrown_small1";
	public static final String METEOR_BROWN_SMALL2_REGION = "meteorBrown_small2";
	public static final String METEOR_BROWN_TINY1_REGION = "meteorBrown_tiny1";
	public static final String METEOR_BROWN_TINY2_REGION = "meteorBrown_tiny2";
	public static final String SILVER_SHEILD_REGION = "shield_silver";

}
